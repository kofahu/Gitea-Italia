# Gitea Italia
L'istanza italiana di Gitea è gestita dal [collettivo devol](https://devol.it), se desiderate aver un account gratuito [contattateci su mastodon](https://mastodon.uno/@devol)

## Perché usare Gitea, l'alternativa etica a Gitlab e Github

Vale la pena ricordare che Github e Gitlab distribuiscono entrambi il software dei loro servizi come software libero. A meno che non si dica altrimenti, questo post riguarda il loro servizio, non il loro software.

### Perchè non usare Gitlab

Il "software libero" che obbliga all'esecuzione di software non libero non è veramente libero. 

* There is nothing particularly wrong with the gitlab software, but that software must be hosted and configured and there are copious ethical problems with the [gitlab.com](http://gitlab.com) service that the OP suggested:
* Sexist treatment toward saleswomen who are [told to wear](https://web.archive.org/web/20200309145121/https://www.theregister.co.uk/2020/02/06/gitlab_sales_women/) dresses, heels, etc.
* Hosted by Google.
* [Proxied](https://about.gitlab.com/blog/2020/01/16/gitlab-changes-to-cloudflare/) through privacy abuser CloudFlare.
* [tracking](https://social.privacytools.io/@darylsun/103015834654172174)
* Hostile treatment of Tor users trying to register.
* Hostile treatment of new users who attempt to register with a @spamgourmet.com forwarding email address to track spam and to protect their more sensitive internal email address.
* Hostile treatment of Tor users after they’ve established an account and have proven to be a non-spammer.

Regarding the last bullet, I was simply trying to edit an existing message that I already posted and was forced to solve a CAPTCHA (attached). There are several problems with this:

* CAPTCHAs break robots and robots are not necessarily malicious. E.g. I could have had a robot correcting a widespread misspelling error in all my posts.
* CAPTCHAs put humans to work for machines when it is machines that should work for humans.
* CAPTCHAs are defeated. Spammers find it economical to use third-world sweat shop labor for CAPTCHAs while legitimate users have this burden of broken CAPTCHAs.
* The reCAPTCHA puzzle requires a connection to Google
*    1. Google’s reCAPTCHAs compromise security as a consequence of surveillance capitalism that entails collection of IP address, browser print.
*    1. * anonymity is [compromised](https://cryptome.org/2016/07/cloudflare-de-anons-tor.htm).
*    1. * (speculative) could Google push malicious j/s that intercepts user registration information?
*    2. Users are forced to execute [non-free javascript](https://libreplanet.org/wiki/Group:Free_Javascript_Action_Team#Ideas_for_focus) ([recaptcha/api.js](https://www.google.com/recaptcha/api.js)).
*    3. The reCAPTCHA requires a GUI, thus denying service to users of text-based clients.
*    4. CAPTCHAs put humans to work for machines when it is machines who should be working for humans. PRISM corp Google Inc. benefits financially from the puzzle solving work, giving Google an opportunity to collect data, abuse it, and profit from it. E.g. Google can track which of their logged-in users are visiting the page presenting the CAPTCHA.
*    5. The reCAPTCHAs are often broken. This amounts to a denial of service. ![gitlab_google_recaptcha](https://user-images.githubusercontent.com/18015852/51769530-9d494300-20e3-11e9-9830-1610b3ae9059.png)
*    
*    5. *  the CAPTCHA server itself refuses to give the puzzle saying there is too much activity.
*    5. *  E.g.2: ![](https://user-images.githubusercontent.com/18015852/55681364-07713600-5926-11e9-8874-137e4faaf423.png)
*    6. The CAPTCHAs are often unsolvable.
*    6. * E.g.1: the CAPTCHA puzzle is broken by ambiguity (is one pixel in a grid cell of a pole holding a street sign considered a street sign?)
*    6. * E.g.2: the puzzle is expressed in a language the viewer doesn’t understand.
*    7. *  (note: for a brief moment [gitlab.com](http://gitlab.com/) switched to hCAPTCHA by Intuition Machines, Inc. but now they’re back to Google’s reCAPTCHA)
*    8. * Network neutrality abuse: there is an access inequality whereby users logged into Google accounts are given more favorable [treatment](https://www.fastcompany.com/90369697/googles-new-recaptcha-has-a-dark-sideby) the CAPTCHA (but then they take on more privacy abuse). Tor users are given extra harsh treatment.

The reason for the reCAPTCHA stuff being hosted on Google.com is shared cookies. This allows reCAPTCHA to gain more information about what you trust Google with online…

This is why gitlab.com should be listed as a service to avoid, like MS [Github](https://github.com).

### Perchè non usare GitHub


#### Privacy problems with Microsoft Github service

1. MS feeds other privacy abusers:
* 1. Github uses Amazon AWS which triggers several privacy and ethical problems
* 2. (2012) MS spent $35 million on Facebook advertisements, making it the third highest financial supporter of a notorious privacy abuser that year.
2. Censorship and project interference: Github staff apparently deleted a contributor who was reporting a privacy abuses present on other projects. Hostility toward volunteer privacy advocates  is in itself sufficient reason to abandon Github.
3. Github may have a policy that entails censoring bug reports (see this post for the discussion)
4. Github is Tor-hostile (according to Tor project, although personally I've had no issue using Tor for GH)
5. MS is a PRISM corporation prone to mass surveillance
6. MS lobbies for privacy-hostile policy:
* 6. MS supported CISPA and CISA unwarranted information exchange bills, and CISA passed.
* 6. (2018) MS paid $195k to fight privacy in CA
7. MS supplies Bing search service which gives high rankings to privacy-abusing CloudFlare websites.
8. MS supplies hotmail.com email service, which uses vigilante extremist org Spamhaus to force residential internet users to share all their e-mail metadata and payloads with a corporate third-party.
9. MS drug tests its employees, thus intruding on their privacy outside the workplace.
10. MS products (Office in particular) violate the GDPR
11. To report an MS security bug, one must sign in and the sign-in page is broken. It's really bad for security to make defect reports difficult to submit.


#### Privacy-compromising consequence of using Github for a project:

1. (conflict of interest) selects only contributors willing to make privacy compromises, and excludes those who will not use GH for privacy reasons.
2. (conflict of interest) When contributors are evaluating whether a tool is privacy-respecting, they white list Microsoft and Amazon as a consequence of using Github, and then use that as rationale to endorse an unworthy tool.
3. (side-effect) Privacy advocates who use GH face demoralizing criticism for what some regard as hypocrisy. PTIO contributors should not be subjected to that.


#### Rationale for staying with Github:

The shake-up of making a move will lose contributors.


#### Problems with Gitlab service

Many Github refugees fled to Gitlab when Microsoft acquired Github. It's a bad idea. Gitlab should be avoided.

#### Alternative

self-hosting Gitea
(+) avoids the "shake-up" problem of shrinking the community each time the project moves (there is no risk that the privacy factors would later take a negative turn).
(+) Gitea.it could host other privacy-focused projects and become part of the support structure for them. Centralizing privacy-focused projects would increase Gitea.it visibility and establish a place where developers with the same high-level goals could develop in a more united way. Poaching privacy-focused projects from GH and GL would solve the hypocrisy problem those projects are facing as well.


You give one-line on the evils and probably not enough detail to be persuasive. 
There’s an enumeration of issues above. Also, most of the projects you recommend have a line “source code: github”. Consider linking to the source code in a way that shames the project, otherwise your site promotes GH more than it discourages it. Not everyone will read the GH section. Perhaps express it this way “source code: github (shamefully)”. Also, prefix “Github” with “MS”. (edit) There is a Github link at the bottom of your page. You should certainly not be linking to it from your public website because it leads visitors in the wrong direction. It also hurts your perceived credibility because many readers won’t follow that link; they will just think “what a hypocrit”. You should set the GH issues to external and link to the gitea.it issues. Your readme is too short. You should use that space as an opportunity to detail all the Github issues I linked you to.